using GTA;
using System;

namespace DisableStuntJumps
{
    public class DisableStuntJumps : Script
    {
        public DisableStuntJumps()
        {
            GTA.Native.Function.Call("SET_POLICE_RADAR_BLIPS", false);
        }
    }
}